// server/server.js
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const port = 8001;

app.use(bodyParser.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));
// server/server.js
// dependencies
// ...
// use dependencies
// ...

// mock data to send to our frontend
let events =
    [
        {
            id: 1,
            name: 'Kissoja',
            category: 'Rahnaa',
            description: 'Jotain jotain',
            featuredImage: 'https://placekitten.com/500/500',
            images: [
                'https://placekitten.com/500/500',
                'https://placekitten.com/500/500',
                'https://placekitten.com/500/500',
            ],
            location: 'Metropolia',
            date: '12-25-2021',
            time: '11:30'
        },
        {
            id: 2,
            name: 'Lisää Kissoja',
            category: 'Adoptiot',
            description: 'Jotain jotain',
            featuredImage: 'https://placekitten.com/500/500',
            images: [
                'https://placekitten.com/500/500'
            ],
            location: 'Karamalmi',
            date: '11-21-2021',
            time: '12:00'
        }
    ];
// server/server.js
// let events = [...]
// ...

// NEW -- get all events
app.get('/events', (req, res) => {
    res.send(events);
});
// server/server.js
app.get('/events/:id', (req, res) => {
    const id = Number(req.params.id);
    const event = events.find(event => event.id === id);
    res.send(event);
});
app.get('/', (req, res) => {
    res.send(`Hi! Server is listening on port ${port}`)
});

// listen on the port
app.listen(port);
